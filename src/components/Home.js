import React, {Component} from 'react';
import {Col, Grid, Row} from "react-bootstrap";

class Home extends Component {

    render() {

        return (
            <div className="App">
                <Grid className="App-input">
                    <Row>
                        <Col xs={8} className="header">Welcome to Artists Unleashed!</Col>
                    </Row>
                    <Row>
                        <Col xs={8}>blah blah blah</Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default Home;