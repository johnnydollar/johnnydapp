// Read-only component displaying app status
import React, {Component} from 'react';
import {Grid, Row, Col} from 'react-bootstrap';

class Status extends Component {

    constructor(props) {
        super(props);
        this.doCanMint = this.canMint.bind(this);
        this.doImageSet = this.imageSet.bind(this);
    }

    canMint() {
        return this.props.doCanMint();
    }

    imageSet() {
        return this.props.doImageSet;
    }

    render() {

        let imageLink, txLink = '';

        if (this.doImageSet) {
            imageLink = <img src={this.props.ipfsImageUrl} alt="" width="100" height="100" />;
        }

        if (this.props.ipfsMetadataHash && this.props.ipfsMetadataHash.trim() !== "") {
            txLink = <a href={''} target="_blank">{this.props.ipfsMetadataHash}</a>
        }

        return (
            <Grid className="App-output">
                <Row>
                    <Col xs={10} className="header">Results</Col>
                </Row>
                <Row>
                    <Col xs={3}>Token Recipient Address</Col>
                    <Col xs={7}>&nbsp;{this.props.recipientAddress}</Col>
                </Row>
                <Row>
                    <Col xs={3}>IPFS Image Hash #</Col>
                    <Col xs={7}>&nbsp;{this.props.ipfsImageHash}</Col>
                </Row>
                <Row>
                    <Col xs={3}>Image File</Col>
                    <Col xs={7}>{imageLink}</Col>
                </Row>
                <Row>
                    <Col xs={3}>IPFS Metadata Hash #</Col>
                    <Col xs={7}>&nbsp;{this.props.ipfsMetadataHash}</Col>
                </Row>
                <Row>
                    <Col xs={3}>IPFS Metadata</Col>
                    <Col xs={7}><pre>{JSON.stringify(this.props.metadataObject, null, 3)}</pre></Col>
                </Row>
                <Row>
                    <Col xs={3}>Tx Hash #</Col>
                    <Col xs={7}>&nbsp;{txLink} </Col>
                </Row>
                {/*
                <Row>
                    <Col xs={3}>Block #</Col>
                    <Col xs={7}>&nbsp;{this.props.blockNumber} </Col>
                </Row>
                <Row>
                    <Col xs={3}>Gas Used</Col>
                    <Col xs={7}>&nbsp;{this.props.gasUsed}</Col>
                </Row>
                */}
            </Grid>
        );
    } //render
}

export default Status;