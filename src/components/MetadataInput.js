// Data input form
import React, {Component} from 'react';
import {Grid, Row, Col, Form} from 'react-bootstrap';

class MetadataInput extends Component {

    constructor(props) {
        super(props);

        this.state = {
            metadata: this.props.metadataObject
        };

        this.handleInputChange = this.onInputChange.bind(this);
    }

    onInputChange(e) {
        const target = e.target;
        const value = target.value;
        let name = target.name;
        let myMetadata = JSON.parse(JSON.stringify(this.state.metadata));

        if (name.startsWith('attribute_')) {
            name = target.name.substring(name.indexOf('_') + 1);
            myMetadata.attributes[name] = value;
        } else {
            myMetadata[name] = value;
        }
        console.log("metadata updated: " + JSON.stringify(myMetadata));

        this.setState({
            metadata: myMetadata
        });

        this.props.updateMetadata(myMetadata);
    }

    render() {

        return (
            <Grid className="App-input">
                <Form>
                    <Row>
                        <Col xs={8} className="header">Item Metadata</Col>
                    </Row>
                    <Row>
                        <Col xs={8} className="sub-header">Elements</Col>
                    </Row>
                    <Row>
                        <Col xs={3}>Name</Col>
                        <Col xs={5}>
                            <input
                                type="text"
                                name="name"
                                style={{width: "350px"}}
                                defaultValue={this.state.metadata.name}
                                onChange={this.handleInputChange}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={3}>Description</Col>
                        <Col xs={5}>
                            <input
                                type="text"
                                name="name"
                                style={{width: "350px"}}
                                defaultValue={this.state.metadata.description}
                                onChange={this.handleInputChange}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={3}>Image URL</Col>
                        <Col xs={5}>
                            <input
                                type="text"
                                name="name"
                                style={{width: "350px"}}
                                defaultValue={this.state.metadata.image}
                                onChange={this.handleInputChange}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={3}>External URL</Col>
                        <Col xs={5}>
                            <input
                                type="text"
                                name="name"
                                style={{width: "350px"}}
                                defaultValue={this.state.metadata.external_url}
                                onChange={this.handleInputChange}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={8} className="sub-header">Attributes</Col>
                    </Row>
                    {this.state.metadata.attributes.map(attribute => (
                    <Row key={'attribute_' + attribute.trait_type}>
                        <Col xs={3}>{attribute.trait_type}</Col>
                        <Col xs={5}>
                            <input
                                type="text"
                                key={'attribute_' + attribute.trait_type}
                                name={'attribute_' + attribute.trait_type}
                                style={{width: "350px"}}
                                defaultValue={attribute.value}
                                onChange={this.handleInputChange}
                            />
                        </Col>
                    </Row>
                    ))}
                </Form>
            </Grid>
        );
    }
}

export default MetadataInput;