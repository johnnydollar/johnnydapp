import React, {Component} from 'react';
import {Col, Grid, Row} from "react-bootstrap";

class Config extends Component {

    constructor(props) {
        super(props);

        this.handleInputChange = this.onInputChange.bind(this);
    }

    onInputChange(e) {
        this.props.handleChange(e);
    }

    render() {

        return (
            <div className="App">
                <Grid className="App-input">
                    <Row>
                        <Col xs={8} className="header">App Settings</Col>
                    </Row>
                    <Row>
                        <Col xs={3}>
                            <a target={'_blank'} href={'https://ipfs.github.io/public-gateway-checker/'}>Update IPFS
                                Gateway</a>
                        </Col>
                        <Col xs={5}>
                            <input
                                type="text"
                                name="ipfsGateway"
                                style={{width: "350px"}}
                                defaultValue={this.props.ipfsGateway}
                                onChange={this.handleInputChange}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={3}>Token Address</Col>
                        <Col xs={5}>
                            <input
                                type="text"
                                name="tokenAddress"
                                style={{width: "350px"}}
                                defaultValue={this.props.tokenAddress}
                                onChange={this.handleInputChange}
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default Config;